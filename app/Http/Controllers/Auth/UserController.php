<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Hash;
use Validator;
use App\Traits\HttpRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    //
    public $successStatus = 200;

    public function login(Request $request){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('nApp')->accessToken;
            return response()->json(['success' => $success], $this->successStatus);
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
        // $rules = [
        //     'grant_type' => 'required|in:client_credentials,password',
        //     'client_id' => 'required',
        //     'client_secret' => 'required',
        //     'scope' => 'required',
        //     'provider' => 'required|in:customers,admin',
        //     'username' => 'required_if:grant_type,password',
        //     'password' => 'required_if:grant_type,password',
        //     'device_type' => 'required_if:grant_type,password',
        //     'device_id' => 'required_if:grant_type,password',
        // ];

        // $messages = [
        //     'grant_type.required' => 'Grant type tidak boleh kosong',
        //     'client_id.required' => 'Client ID tidak boleh kosong',
        //     'client_secret.required' => 'Client Secret tidak boleh kosong',
        //     'scope.required' => 'Scope tidak boleh kosong',
        //     'provider.required' => 'Provider tidak boleh kosong',
        //     'username.required_if' => 'Username tidak boleh kosong.',
        //     'password.required_if' => 'Password tidak boleh kosong.',
        //     'device_type.required_if' => 'Device Type tidak boleh kosong.',
        //     'device_id.required_if' => 'Device ID tidak boleh kosong.',
        // ];

        // $validate = Validator::make($request->all(), $rules, $messages);

        // if (!$validate) {
        //     return $validate->errors()->first();
        // }

        // return $res = $this->sendRequest('POST', env('APP_API') . 'oauth/token', null, $request, null, null);
    }
    //     if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
    //         $user = Auth::user();
    //         $success['token'] =  $user->createToken('nApp')->accessToken;
    //         return response()->json(['success' => $success], $this->successStatus);
    //     }
    //     else{
    //         return response()->json(['error'=>'Unauthorised'], 401);
    //     }
    // }

    public function register(Request $request)
    {
        $rules = [
            'email' => 'required|string|email|max:190|unique:users,email',
            'password' => 'required|regex:/^[0-9]+$/|confirmed',
            'name' => 'required',
        ];
        
        $messages= [
            'email.required' => 'Grant type tidak boleh kosong',
            'password.required' => 'Client ID tidak boleh kosong',
            'name.required' => 'Client Secret tidak boleh kosong'
        ];
        $validate = Validator::make($request->all(), $rules, $messages);

        if (!$validate) {
            return $validate->errors()->first();
        }

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('nApp')->accessToken;
        $success['name'] =  $user->name;

        return response()->json(['success'=>$success], $this->successStatus);
    //
    }
    
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }
    public function valid(Validation $v){
        return $v;
    }
}
