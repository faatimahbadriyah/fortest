<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Test extends Controller
{
    public function index(){
        return response()->json(['code' => 200, 'message' => 'success', 'data' => 'api enabled!']);
    }
}
