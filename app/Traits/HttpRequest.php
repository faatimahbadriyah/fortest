<?php

namespace App\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

trait HttpRequest
{
    /**
     * Create http request
     *
     * @param string $method
     * @param string $url
     * @param array $headers
     * @param array $formParams
     * @param array $query
     * @param array $json
     * @return void
     */
    public function sendRequest($method, $url, $headers, $formParams, $query, $json)
    {
        try {
            $client = new Client;
            $res = $client->request($method, $url, [
                'headers' => $headers,
                'form_params' => $formParams,
                'query' => $query,
                'json' => $json,
            ]);
            return json_decode($res->getBody(), true);
        } catch (RequestException $e) {
            return json_decode($e->getResponse()->getBody()->getContents(), true);
        }
    }
}
