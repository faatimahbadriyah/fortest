<?php

namespace App\Traits;

use Illuminate\Support\Facades\Validator;

trait Validation
{
    /**
     * Create validation
     *
     * @param array $request
     * @param array $rules
     * @param string $messages
     * @return void
     */
    public function validator($request, $rules, $messages)
    {
        $validator = Validator::make($request, $rules, $messages);

        if ($validator->fails()) {
            return $validator->errors()->first();
        } else {
            return false;
        }
    }
}
