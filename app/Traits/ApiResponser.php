<?php

namespace App\Traits;

use Illuminate\Http\Response;

trait ApiResponser
{
    /**
     * Create success response
     *
     * @param integer $code
     * @param string $message
     * @param array $data
     * @return void
     */
    public function successResponse($code, $message, $data)
    {
        return response()->json(['code' => $code, 'message' => $message, 'data' => $data], $code);
    }

    /**
     * Create error response
     *
     * @param integer $code
     * @param string $message
     * @param array $data
     * @return void
     */
    public function errorResponse($code, $message, $data)
    {
        return response()->json(['code' => $code, 'message' => $message, 'data' => $data], $code);
    }

    /**
     * Create paginate response
     *
     * @param integer $code
     * @param string $message
     * @param array $data
     * @return void
     */
    public function paginateResponse($code, $message, $data)
    {
        $pagination = [
            'total' => $data->total(),
            'count' => $data->lastItem(),
            'per_page' => (int) $data->perPage(),
            'current_page' => $data->currentPage(),
            'total_pages' => $data->lastPage(),
            'links' => [
                'first_page' => $data->url(1),
                'last_page' => $data->url($data->lastPage()),
                'next_page' => $data->nextPageUrl(),
                'prev_page' => $data->previousPageUrl(),
            ],
        ];
        return response()->json(['code' => $code, 'message' => $message, 'data' => $data->items(), 'pagination' => $pagination], $code);
    }
}
