<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\Test as tes;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', 'App\Http\Controllers\Test@index');
// Route::get('/', 'Test@index');
Route::post('login', 'App\Http\Controllers\Auth\UserController@login');
Route::post('register', 'App\Http\Controllers\Auth\UserController@register');

Route::group(['middleware' => 'auth:api'], function(){
	Route::post('details', 'Auth/UserController@details');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
